module F = Farith.F

let fpp mode fmt q = F.pp fmt (F.of_q mode ~mw:52 ~ew:11 q)

let () =
  let f = F.of_float 3.1 in
  Format.printf "[F]  3.1 = %a@." F.pp f;
  let q = Q.make (Z.of_int 31) (Z.of_int 10) in
  Format.printf "[Fp] 3.1 = %a@." F.pp
    (F.round ~mw:24 ~ew:11 ZR (F.of_float 3.1));
  Format.printf "[Fq] 3.1 = %a@." F.pp (F.of_q ~mw:24 ~ew:11 ZR q);
  Format.printf "-----------------------@.";
  Format.printf " Simple Roundings@.";
  let job m m2 q =
    Format.printf "-----------------------@.";
    Format.printf "Q=%a@." Q.pp_print q;
    Format.printf "[F-%s] %a@." m (fpp m2) q;
    Format.printf "[F-%s] %a@." m (fpp m2) (Q.neg q)
  in
  job "NE" Farith.Mode.NE q;
  job "NA" Farith.Mode.NA q;
  job "ZR" Farith.Mode.ZR q;
  job "UP" Farith.Mode.UP q;
  job "DN" Farith.Mode.DN q;
  Format.printf "-----------------------@.";
  Format.printf " Tie Breaks (NE)@.";
  let eps = Z.shift_left Z.one 51 in
  let e_ex = Q.make (Z.of_int 0b100) eps in
  let e_lo = Q.make (Z.of_int 0b101) eps in
  let e_ti = Q.make (Z.of_int 0b110) eps in
  let e_up = Q.make (Z.of_int 0b111) eps in
  job "NE-ex" Farith.Mode.NE (Q.add Q.one e_ex);
  job "NE-lo" Farith.Mode.NE (Q.add Q.one e_lo);
  job "NE-ti" Farith.Mode.NE (Q.add Q.one e_ti);
  job "NE-up" Farith.Mode.NE (Q.add Q.one e_up);
  Format.printf "-----------------------@.";
  Format.printf " Tie Breaks (NA)@.";
  job "NA-ex" Farith.Mode.NA (Q.add Q.one e_ex);
  job "NA-lo" Farith.Mode.NA (Q.add Q.one e_lo);
  job "NA-ti" Farith.Mode.NA (Q.add Q.one e_ti);
  job "NA-up" Farith.Mode.NA (Q.add Q.one e_up)
